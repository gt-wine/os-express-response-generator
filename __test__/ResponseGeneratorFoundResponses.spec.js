
const res = require('./mocks/res');
const ResponseGenerator = require('../index');

describe('ResponseGeneratorFound', () => {
  test('Data Array should call res.json', () => {
    const responseGenerator = Object.create(ResponseGenerator);
    responseGenerator.generateResponseWithData(res, [{ data: 'mock data' }]);
    expect(res.json.mock.calls.length).toBe(1);
    expect(res.status.mock.calls[0][0]).toBe(200);
    expect(res.status.mock.calls.length).toBe(1);
  });
  test('Empty Data Array should call res.json', () => {
    const responseGenerator = Object.create(ResponseGenerator);
    responseGenerator.generateResponseWithData(res, []);
    expect(res.json.mock.calls.length).toBe(2);
    expect(res.status.mock.calls[1][0]).toBe(404);
    expect(res.status.mock.calls.length).toBe(2);
  });
  test('Data Array should call found', () => {
    const responseGenerator = Object.create(ResponseGenerator);
    responseGenerator.createFoundResponseWithData = jest.fn();
    responseGenerator.createNotFoundResponse = jest.fn();
    responseGenerator.generateResponseWithData(res, [{ data: 'mock data' }]);
    expect(responseGenerator.createFoundResponseWithData.mock.calls.length).toBe(1);
    expect(responseGenerator.createNotFoundResponse.mock.calls.length).toBe(0);
  });
  test('Empty Data Array should not call found', () => {
    ResponseGenerator.createFoundResponseWithData = jest.fn();
    ResponseGenerator.generateResponseWithData(res, []);
    expect(ResponseGenerator.createFoundResponseWithData.mock.calls.length).toBe(0);
  });
  test('Data Object should call found', () => {
    const responseGenerator = Object.create(ResponseGenerator);
    responseGenerator.createFoundResponseWithData = jest.fn();
    responseGenerator.createNotFoundResponse = jest.fn();
    responseGenerator.generateResponseWithData(res, { data: 'mock data object' });
    expect(responseGenerator.createFoundResponseWithData.mock.calls.length).toBe(1);
    expect(responseGenerator.createNotFoundResponse.mock.calls.length).toBe(0);
  });
  test('Empty Data Object should not call found', () => {
    ResponseGenerator.createFoundResponseWithData = jest.fn();
    ResponseGenerator.generateResponseWithData(res, {});
    expect(ResponseGenerator.createFoundResponseWithData.mock.calls.length).toBe(0);
  });
  test('Null Data Object should not call found', () => {
    ResponseGenerator.createFoundResponseWithData = jest.fn();
    ResponseGenerator.generateResponseWithData(res, null);
    expect(ResponseGenerator.createFoundResponseWithData.mock.calls.length).toBe(0);
  });
});

const res = {
  status: jest.fn(),
  json: jest.fn(),
  httpStatusCodes: {
    OK: 200,
    NOT_FOUND: 404,
    INTERNAL_SERVER_ERROR: 500,
  },
};
res.status.mockReturnValue(res);

module.exports = res;

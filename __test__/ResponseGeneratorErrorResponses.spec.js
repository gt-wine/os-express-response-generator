const res = require('./mocks/res');
const AppError = require('./mocks/AppError');
const ResponseGenerator = require('../index');

describe('ResponseGeneratorError', () => {
  test('Generate Error response with Error', async () => {
    await ResponseGenerator.generateResponseWithError(res, new Error('mock error message'));
    expect(res.status.mock.calls.length).toBe(1);
    expect(res.status.mock.calls[0][0]).toBe(500);
    expect(res.status().json.mock.calls.length).toBe(1);
    // validate response
    expect(res.json.mock.calls[0][0].meta).toBeDefined();
    expect(res.json.mock.calls[0][0].meta.code).toBe(500);
    expect(res.json.mock.calls[0][0].meta.message).toBe('mock error message');
  });
  test('Generate Error response with custom Error with status meta', async () => {
    await ResponseGenerator.generateResponseWithError(res, new AppError('mock error message', { statusCode: 404, errors: ['error 1'] }));
    expect(res.status.mock.calls.length).toBe(3);
    expect(res.status.mock.calls[2][0]).toBe(404);
    expect(res.status().json.mock.calls.length).toBe(2);
    // validate response
    expect(res.json.mock.calls[1][0].meta).toBeDefined();
    expect(res.json.mock.calls[1][0].meta.code).toBe(404);
    expect(res.json.mock.calls[1][0].meta.message).toBe('mock error message');
    expect(Array.isArray(res.json.mock.calls[1][0].meta.errors)).toBe(true);
  });
  test('Try Generate Error response with null', async () => {
    try {
      await ResponseGenerator.generateResponseWithError(res, null);
    } catch (error) {
      expect(error.message).toBe('error must be and instance of Error');
    }
  });
});


const ResponseGenerator = require('../index');
const res = require('./mocks/res');

beforeAll(() => {});

describe('ResponseGeneratorNotFound', () => {
  test('Empty Data should call not found', async () => {
    ResponseGenerator.createNotFoundResponse = jest.fn();
    await ResponseGenerator.generateResponseWithData(res, []);
    expect(ResponseGenerator.createNotFoundResponse.mock.calls.length).toBe(1);
  });
});

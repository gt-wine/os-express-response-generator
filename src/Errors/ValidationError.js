class ValidationError extends Error {
  constructor(message, options) {
    super(message);
    const keys = Object.keys(options);
    for (let i = 0, max = keys.length; i < max; i += 1) {
      this[keys[i]] = options[keys[i]];
    }
  }
}
module.exports = ValidationError;

# Winecraft Express Response Generator


## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Versioning](#versioning)
- [Support](#support)
- [Contributing](#contributing)

## Installation

### General Setup
```
$ npm install
```

## Usage

simple http JSON response generator for express.
```
const express = require('express');
const EntityService = require('./Services/EntityService');
const ResponseGenerator = require('winecraft-os-express-response-generator');
/**
 * Instantiate the App
 */
app.get(`/entities`, (req, res) {
  try {
    const data = await EntityService.getEntities(req);
    return ResponseGenerator.generateResponseWithData(res, data);
  } catch(error) {
    return ResponseGenerator.generateResponseWithError(res, error);
  }
});
```

## Versioning

This project uses semantic versioning - https://semver.org/

To create a new version run the following command

```
$ npm adduser
$ npm version {major|minor|patch}
$ npm publish
```

## Support

Adam Synnott <adam.synnott@winecraft.com>

## Contributing

Please contribute using [Github Flow](https://guides.github.com/introduction/flow/).

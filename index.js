module.exports = {
  /**
   * Generate an empty response that's still OK
   * @param {Object} res
   */
  generateEmptyResponse(res) {
    res.status(res.httpStatusCodes.OK).json({
      meta: {
        code: res.httpStatusCodes.OK,
        message: 'ok',
        description: null,
      },
      data: null,
    });
  },
  /**
   *
   * @param {*} res
   * @param {*} data
   */
  generateResponseWithData(res, data) {
    let dataIsEmpty = null;
    if (!data) {
      dataIsEmpty = true;
    } else if (Array.isArray(data)) {
      dataIsEmpty = data.length < 1;
    } else if (typeof data === 'object') {
      dataIsEmpty = Object.keys(data).length < 1;
    }
    if (dataIsEmpty || !data) {
      return this.createNotFoundResponse(res);
    }
    return this.createFoundResponseWithData(res, data);
  },
  /**
   *
   * @param {*} res
   */
  createNotFoundResponse(res) {
    res.status(res.httpStatusCodes.NOT_FOUND).json({
      meta: {
        code: res.httpStatusCodes.NOT_FOUND,
        message: 'Not found',
        description: null,
      },
      data: null,
    });
  },
  /**
   *
   * @param {*} res
   * @param {*} data
   */
  createFoundResponseWithData(res, data) {
    res.status(res.httpStatusCodes.OK).json({
      meta: {
        code: res.httpStatusCodes.OK,
        message: 'ok',
        description: null,
      },
      data,
    });
  },
  /**
   *
   * @param {*} res
   * @param {*} error
   */
  generateResponseWithError(res, error) {
    if (!(error instanceof Error)) {
      throw new Error('error must be and instance of Error');
    }

    const statusCode = error.statusCode || res.httpStatusCodes.INTERNAL_SERVER_ERROR;

    return res.status(statusCode).json({
      meta: {
        code: statusCode,
        message: error.message,
        description: error.description || null,
        errors: error.errors || null,
      },
    });
  },
};

